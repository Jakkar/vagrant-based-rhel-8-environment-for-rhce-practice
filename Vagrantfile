# Vagrant file for setting up an environment to use for "Ansible Sample Exam for RHCE EX294 and EX407" on Lisenet
# https://www.lisenet.com/2019/ansible-sample-exam-for-ex294/

ENV['VAGRANT_EXPERIMENTAL'] = 'typed_triggers'
require 'open3'

Vagrant.configure("2") do |config|

  # grr fedora packaging an old version of vagrant-libvirt
  # wacky workaround: https://github.com/vagrant-libvirt/vagrant-libvirt/pull/692#issuecomment-922329049
  config.vm.provider :libvirt do |lv, config|
    config.vm.provider :libvirt do |lv, config|
      lv.storage :file, :size => '3G', :device => 'sda', :bus => 'scsi', :discard => 'unmap', :cache => 'unsafe'
      config.trigger.before :'VagrantPlugins::ProviderLibvirt::Action::StartDomain', type: :action do |trigger|
        trigger.ruby do |env, machine|
          stdout, stderr, status = Open3.capture3(
            'virt-xml', machine.id,
            '--edit', 'type=scsi',
            '--controller', 'model=virtio-scsi')
          if status.exitstatus != 0
            raise "failed to run virt-xml to modify the controller model. status=#{status.exitstatus} stdout=#{stdout} stderr=#{stderr}"
          end
        end
      end
    end

    # use system session instead of user session for vms
    lv.qemu_use_session = false
  end

  #################################################################################################

   # Control node
   config.vm.define :ansible_control_node do |node|
    node.vm.box = "roboxes/rhel8"
    node.vm.hostname = "ansible-control.hl.local"
    node.vm.network :private_network, :ip => "10.0.0.1"
    node.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".git/"

    node.vm.provision :shell, 
      :name => "Add pregenerated ssh priv key as priv key for root user",
      :inline => "mkdir -p /root/.ssh; sudo cp /vagrant/keys/key /root/.ssh/id_rsa", run: "always"

    # indentation has to be wierd for multiline stuff to to work :(
    node.vm.provision :shell,
      :name => "Populate /etc/hosts",
      :inline => <<-SHELL
      cat << 'EOF' >> /etc/hosts
\n
10.0.0.1 ansible-control.hl.local ansible-control
10.0.0.2 ansible2.hl.local ansible2
10.0.0.3 ansible3.hl.local ansible3
10.0.0.4 ansible4.hl.local ansible4
10.0.0.5 ansible5.hl.local ansible5
EOF
SHELL
  end

  # Ansible nodes
  (2..5).each do |i|
    config.vm.define "ansible_node#{i}" do |node|
      node.vm.box = "roboxes/rhel8"
      node.vm.hostname = "ansible#{i}.hl.local"
      node.vm.network :private_network, :ip => "10.0.0.#{i}"
      node.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".git/"

      node.vm.provision :shell, 
        :name => "Add pregenerated ssh pub key as authorized key for root user",
        :inline => "mkdir -p /root/.ssh; sudo cp /vagrant/keys/key.pub /root/.ssh/authorized_keys", run: "always"
      
      # node 5 is to have an extra disk
      node.vm.provider :libvirt do |libvirt|
        if "#{i}" == "5"
          libvirt.storage :file, :size => '1G'
        end
      end
    end
  end
end
