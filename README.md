# Vagrant based RHEL 8 environment for RHCE practice
Made to fit the requirements from  the Lisenet ["Ansible Sample Exam for RHCE EX294 and EX407"](https://www.lisenet.com/2019/ansible-sample-exam-for-ex294/) so it's easier to get started.

Made for Linux with the libvirt provider. Slightly cursed, but works on my matchine™. 

> Not tested on anything other than Fedora 36. Includes a dirty workaround for Fedora packaging an old version of vagrant-libvirt, so it may not work in the future or on a different distro?

## Notes

- The extra disk on `ansible5.hl.local` is /dev/**v**db instead of /dev/**s**db

## Requirements

- Linux
- Libvirt, QEMU, KVM ... stuff
- Vagrant and [vagrant-libvirt](https://github.com/vagrant-libvirt/vagrant-libvirt).

## Use

```
vagrant up --provider=libvirt --no-parallel
```
> The `--no-parallel` option is yet again a workaround for Fedora packaging an old vagrant-libvirt version. Using it has no downsides apart from provisioning being slow, but you can try removing it and see if it works fine for you :)

SSH into `ansible_control_node` with
```
vagrant ssh ansible_control node
```
Become root with 
```
sudo -i
```
And start from there. 

Passwordless ssh to root on the other nodes is pre-configured. 

`/etc/hosts` has been modified so it should be no problem to use the FQDNs specified on the Lisenet page. 